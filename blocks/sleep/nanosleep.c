#include <stdio.h>
#include <stdlib.h>
#include <ubx/ubx.h>

//======================================
char block_meta[] =
	"{ doc='just output a blockuence of number, usefull for testing purpose',"
	"  real-time=true,"
	"}";
ubx_port_t block_ports[] = {
	{ .name="out", .out_type_name="unsigned int" },
	{ .name="in", .in_type_name="unsigned int" },
	{ NULL },
};
struct block_info {
};
def_write_fun(write_uint, unsigned int)
def_read_fun(read_uint, unsigned int)
//========================================
static int block_init(ubx_block_t *b)
{
	int ret=0;
	DBG(" ");
	if ((b->private_data = calloc(1, sizeof(struct block_info)))==NULL) {
		ERR("Failed to alloc memory");
		ret=EOUTOFMEM;
		goto out;
	}
 out:
	return ret;
}
static void block_cleanup(ubx_block_t *b)
{
	DBG(" ");
	free(b->private_data);
}
static int block_start(ubx_block_t *b)
{
	return 0; /* Ok */
}
static void block_step(ubx_block_t *b) {
	ubx_port_t* in_port = ubx_port_get(b, "in");
	ubx_port_t* out_port = ubx_port_get(b, "out");
	
	unsigned int data;
	read_uint(in_port, &data);
	write_uint(out_port, &data);
}
ubx_block_t block_comp = {
	.name = "copy/copy",
	.type = BLOCK_TYPE_COMPUTATION,
	.meta_data = block_meta,
	.ports = block_ports,

	/* ops */
	.init = block_init,
	.start = block_start,
	.step = block_step,
	.cleanup = block_cleanup,
};

/**
 * rnd_module_init - initialize module
 *
 * here types and blocks are registered.
 *
 * @param ni
 *
 * @return 0 if OK, non-zero otherwise (this will prevent the loading of the module).
 */
static int block_module_init(ubx_node_info_t* ni)
{
	return ubx_block_register(ni, &block_comp);
}

/**
 * rnd_module_cleanup - de
 *
 * unregister blocks.
 *
 * @param ni
 */
static void block_module_cleanup(ubx_node_info_t *ni)
{
	ubx_type_unregister(ni, "struct block_config");
	ubx_block_unregister(ni, "block/block");
}

/* declare the module init and cleanup function */
UBX_MODULE_INIT(block_module_init)
UBX_MODULE_CLEANUP(block_module_cleanup)
UBX_MODULE_LICENSE_SPDX(BSD-3-Clause)
