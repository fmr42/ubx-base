#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <time.h>

#include <ubx/ubx.h>

#include "types/sleep_config.h"
#include "types/sleep_config.h.hexarr"

ubx_type_t sleep_config_type = def_struct_type(struct sleep_config, &sleep_config_h);

char block_meta[] =
	"{ doc='Sleep for the given amount of time',"
	"  real-time=true,"
	"}";
ubx_config_t block_config[] = {
	{ .name="sleep_time", .type_name = "struct sleep_config" },
	{ NULL },
};

ubx_port_t block_ports[] = {
	{ NULL },
};
struct block_info {
	int sec;
	int nsec;
};
static int block_init(ubx_block_t *b)
{
	int ret=0;
	if ((b->private_data = calloc(1, sizeof(struct block_info)))==NULL) {
		ERR("Failed to alloc memory");
		ret=EOUTOFMEM;
		goto out;
	}
 out:
	return ret;
}
static void block_cleanup(ubx_block_t *b)
{
	free(b->private_data);
}
static int block_start(ubx_block_t *b)
{
	unsigned int clen;
	struct sleep_config* cfg;
	struct block_info*   inf;

	inf=(struct block_info*) b->private_data;
	cfg = (struct sleep_config*) ubx_config_get_data_ptr(b, "sleep_time", &clen);
	
	inf->sec = cfg->sec;
	inf->nsec = cfg->nsec;

	return 0; /* Ok */
}
static void block_step(ubx_block_t *b) {
	struct block_info* inf;
	inf=(struct block_info*) b->private_data;

	struct timespec d;
	d.tv_sec = inf->sec;
	d.tv_nsec = inf->nsec;
	printf("Sleeping for %d sec and %d nsec...",(int)d.tv_sec, (int)d.tv_nsec);
	fflush(stdout);
	nanosleep(&d,NULL);
	printf("done\n");
	
}
ubx_block_t sleep_comp = {
	.name = "sleep/sleep",
	.type = BLOCK_TYPE_COMPUTATION,
	.meta_data = block_meta,
	.configs = block_config,
	.ports = block_ports,

	/* ops */
	.init = block_init,
	.start = block_start,
	.step = block_step,
	.cleanup = block_cleanup,
};
static int this_module_init(ubx_node_info_t* ni)
{
	ubx_type_register(ni, &sleep_config_type);
	return ubx_block_register(ni, &sleep_comp);
}
static void this_module_cleanup(ubx_node_info_t *ni)
{
	ubx_type_unregister(ni, "struct sleep_config");
	ubx_block_unregister(ni, "sleep/sleep");
}

/* declare the module init and cleanup function */
UBX_MODULE_INIT(this_module_init)
UBX_MODULE_CLEANUP(this_module_cleanup)
UBX_MODULE_LICENSE_SPDX(BSD-3-Clause)
