/*
 * Output a sequence of numbers: 0, 1, 2, 3, 4, ...
 * of type int
 */

/* #define DEBUG 1 */

#include <stdio.h>
#include <stdlib.h>

#include <ubx/ubx.h>


/* function block meta-data
 * used by higher level functions.
 */
char seq_meta[] =
	"{ doc='just output a sequence of number, usefull for testing purpose',"
	"  real-time=true,"
	"}";


/* Ports
 */
ubx_port_t seq_ports[] = {
	{ .name="out", .out_type_name="unsigned int" },
	{ NULL },
};

struct seq_info {
	int counter;
};

/* convenience functions to read/write from the ports these fill a
 * ubx_data_t, and call port->[read|write](&data). These introduce
 * some type safety.
 */
def_write_fun(write_uint, unsigned int)

/**
 * seq_init - block init function.
 *
 * for RT blocks: any memory should be allocated here.
 *
 * @param b
 *
 * @return Ok if 0,
 */
static int seq_init(ubx_block_t *b)
{
	int ret=0;
	DBG(" ");
	if ((b->private_data = calloc(1, sizeof(struct seq_info)))==NULL) {
		ERR("Failed to alloc memory");
		ret=EOUTOFMEM;
		goto out;
	}
 out:
	return ret;
}

/**
 * seq_cleanup - cleanup block.
 *
 * for RT blocks: free all memory here
 *
 * @param b
 */
static void seq_cleanup(ubx_block_t *b)
{
	DBG(" ");
	free(b->private_data);
}

/**
 * seq_start - start the random block.
 *
 * @param b
 *
 * @return 0 if Ok, if non-zero block will not be started.
 */
static int seq_start(ubx_block_t *b)
{
	DBG("in");
	struct seq_info* inf;

	inf=(struct seq_info*) b->private_data;
	inf->counter = 0;

	return 0; /* Ok */
}

/**
 * seq_step - this function implements the main functionality of the
 * block. Ports are read and written here.
 *
 * @param b
 */
static void seq_step(ubx_block_t *b) {
	struct seq_info* inf;
	inf=(struct seq_info*) b->private_data;
	inf->counter++;
	
	ubx_port_t* out_port = ubx_port_get(b, "out");
	
	unsigned int out = inf->counter;
	write_uint(out_port, &out);

}


/* put everything together
 *
 */
ubx_block_t seq_comp = {
	.name = "seq/seq",
	.type = BLOCK_TYPE_COMPUTATION,
	.meta_data = seq_meta,
	.ports = seq_ports,

	/* ops */
	.init = seq_init,
	.start = seq_start,
	.step = seq_step,
	.cleanup = seq_cleanup,
};

/**
 * rnd_module_init - initialize module
 *
 * here types and blocks are registered.
 *
 * @param ni
 *
 * @return 0 if OK, non-zero otherwise (this will prevent the loading of the module).
 */
static int seq_module_init(ubx_node_info_t* ni)
{
	return ubx_block_register(ni, &seq_comp);
}

/**
 * rnd_module_cleanup - de
 *
 * unregister blocks.
 *
 * @param ni
 */
static void seq_module_cleanup(ubx_node_info_t *ni)
{
	ubx_type_unregister(ni, "struct seq_config");
	ubx_block_unregister(ni, "seq/seq");
}

/* declare the module init and cleanup function */
UBX_MODULE_INIT(seq_module_init)
UBX_MODULE_CLEANUP(seq_module_cleanup)
UBX_MODULE_LICENSE_SPDX(BSD-3-Clause)
