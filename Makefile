include Makefile.conf

DIRS=$(wildcard blocks/* types/*)
BUILDDIRS = $(DIRS:%=build-%)
INSTALLDIRS = $(DIRS:%=install-%)
CLEANDIRS = $(DIRS:%=clean-%)

all: $(BUILDDIRS) 

$(DIRS): $(BUILDDIRS)

$(BUILDDIRS):
	make -C $(@:build-%=%)

install: $(INSTALLDIRS) all
$(INSTALLDIRS):
	make -C $(@:install-%=%) install

clean: $(CLEANDIRS)
$(CLEANDIRS):
	make -C $(@:clean-%=%) clean

.PHONY: subdirs $(DIRS)
.PHONY: subdirs $(BUILDDIRS)
.PHONY: subdirs $(INSTALLDIRS)
.PHONY: subdirs $(CLEANDIRS)
.PHONY: all install clean

